<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

require_once __DIR__.'/config.inc.php';
define ('GESTION',__DIR__);

// Menus
$a_path = array(
    'tag'=>array( 'label'=>'Etiquette','file'=>'tag.inc.php'),
    'dossier'=>array( 'label'=>'Dossier','file'=>'dossier.inc.php'),
    'mod'=>array( 'label'=>'Modèle','file'=>'modele.inc.php'),
    'teach'=>array( 'label'=>'Professeur','file'=>'teacher.inc.php'),
    'student'=>array("label"=>"Etudiant",'file'=>'student.inc.php'),
    'log'=>array( 'label'=>'Utilisation','file'=>'log.inc.php')
      
);

$a_ajax = array(
  // Display a tag
    'ajax_tag_display'=>'ajax_tag.php'  
  , // insert a new tag
    'ajax_tag_insert'=>'ajax_tag.php' 
  ,// Save a tag 
    'ajax_tag_save'=>'ajax_tag.php'   
  , // delete a tag  
    'ajax_tag_delete'=>'ajax_tag.php' 
  , // show a list of tags for selection
    'ajax_tag_select'=>'ajax_tag.php' 
  , // set the tag on a folder or a template
    'ajax_tag_set'=>'ajax_tag.php' 
  , // Add a tag to a teacher
    'teacher_tag_add'=>'ajax_teacher.php' 
  ,// remove a tag from a teacher
    'teacher_tag_remove'=>'ajax_teacher.php' 
  , // Display available tag to add
  'teacher_tag_select'=>'ajax_teacher.php' 
  ,// Confirm drop of database
    'ajax_drop_confirm'=>'ajax_database.php' 
  ,// Drop of db is confirmed
    'ajax_drop'=>'ajax_database.php' 
  ,// display a form to choose a new Teacher
    'ajax_new_teacher'=>"ajax_teacher.php" 
  ,//add new teacher 
    'ajax_add_teacher'=>"ajax_teacher.php" 
  ,// remove a teacher
    "ajax_remove_teacher"=>"ajax_teacher.php" 
  , // Find a teacher from autocomplete 
    "findTeacher"=>"ajax_teacher.php"
  , // display a list of student for confirmation
    "ajax_remove_student"=>"ajax_student.php"
);
global $a_path;
if ( ! defined('ECOLE')) {
    define ('ECOLE',"");
}
