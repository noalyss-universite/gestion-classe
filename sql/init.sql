create schema extended_admin;

CREATE TABLE extended_admin.inscription (
	id  SERIAL NOT NULL, 
	ins_ip_addr text, 
	ins_date_subscribe timestamp default now(), 
	ins_email text, 
	ins_name text, 
	ins_first_name text, 
	ins_tag text, 
	ins_date_last_result timestamp, 
	ins_result_process text, 
	ins_flag_result char(1) constraint ck_flag_result check (ins_flag_result in  ('E','S','P')), 
	use_id int4, 
	mod_id int4,
	dos_id int4,
	
	PRIMARY KEY (id));
COMMENT ON TABLE extended_admin.inscription IS 'Contient toutes les tentatives d''inscription ainsi que leur résultat';
COMMENT ON COLUMN extended_admin.inscription.use_id IS 'si différent de null , alors ac_users existe';
comment on column extended_admin.inscription.mod_id is 'modele utilisé pour le dossier';
comment on column extended_admin.inscription.ins_ip_addr is 'Addr IP du demandeur d''inscription';
comment on column extended_admin.inscription.ins_date_subscribe is 'Date de demande d''inscription';
comment on column extended_admin.inscription.ins_tag is 'Etiquette choisie lors inscription';
comment on column extended_admin.inscription.ins_date_last_result  is 'Date fin du process ';
comment on column extended_admin.inscription.ins_result_process  is 'Messages du processe ';
comment on column extended_admin.inscription.ins_flag_result  is 'resultat process E Error S success P en cours';
comment on column extended_admin.inscription.use_id  is 'utilisateur ';
comment on column extended_admin.inscription.mod_id  is 'Modèle utilisé';
comment on column extended_admin.inscription.dos_id  is 'Dossier crée ';

create table extended_admin.tag (
    id serial primary key,
    tag_code text ,
    tag_description text
);

comment on table extended_admin.tag is 'Etiquettes utilisables';
comment on column extended_admin.tag.tag_code is 'Code étiquette';
comment on column extended_admin.tag.tag_description is 'Note interne';
create unique index  tag_code_ux on extended_admin.tag(lower(tag_code));


create table extended_admin.dossier_tag (
    id serial primary key,
    tag_id bigint references extended_admin.tag(id) on delete cascade ,
    dos_id bigint references public.ac_dossier(dos_id) on delete cascade on update cascade,
    dt_drop char(1) default 'N' constraint dt_drop_ck check (dt_drop in ('Y','N'))
);

create unique index  dos_id_ux on extended_admin.dossier_tag(dos_id);

comment on table extended_admin.dossier_tag is 'Join between ac_dossier and tag';
comment on column extended_admin.dossier_tag.dt_drop is 'Y for dossier to drop';

create table extended_admin.modele_tag (
    id serial primary key,
    tag_id bigint references extended_admin.tag(id) on delete cascade ,
    mod_id bigint references public.modeledef(mod_id) on delete cascade on update cascade,
    mt_drop char(1) default 'N' constraint mt_drop_ck check (mt_drop in ('Y','N'))
);
comment on table extended_admin.dossier_tag is 'Join between modeledef and tag';
comment on column extended_admin.dossier_tag.dt_drop is 'Y for modele to drop';

create unique index  tag_id_ux on extended_admin.modele_tag(tag_id);


create table extended_admin.teacher_tag (
    id serial primary key,
    tag_id bigint references extended_admin.tag(id) on delete cascade ,
    use_id bigint references public.ac_users(use_id) on delete cascade on update cascade
);

create unique index  teacher_tag_ux on extended_admin.teacher_tag(tag_id,use_id);



create table extended_admin.version (id bigint primary key , version_date timestamp default now(),version_note text);
insert into extended_admin.version (id,version_note)values (1,'Installation');
commit;