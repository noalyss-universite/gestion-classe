begin;
create table extended_admin.teacher ( use_id bigint primary key );
alter table extended_admin.teacher add  constraint ac_login_teacher_fk foreign key  (use_id) references ac_users(use_id) on delete cascade on update cascade;
insert into extended_admin.version (id,version_note)values (2,'Add table teacher to add and remove teacher');
commit;
