begin;
alter table extended_admin.teacher_tag drop constraint teacher_tag_use_id_fkey;
alter table extended_admin.teacher_tag add constraint teacher_tag_use_id_fkey foreign key(use_id) references extended_admin.teacher(use_id) on delete cascade on update cascade ;
insert into extended_admin.version (id,version_note)values (3,'Add FK on teacher_tag');
commit;
