
var posX=0,posY=0,offsetX=10,offsetY=10;
document.onmousemove=getPosition;

function getValue(xml,p_tag) {
    return getNodeText(xml.getElementsByTagName(p_tag)[0])
}
function warning(p_string) {
    $('warning_div').setStyle='z-index:15';
    $('warning_div').innerHTML=p_string;
    new Effect.Appear('warning_div');
    new Effect.Fade('warning_div');
    $('warning_div').setStyle='z-index:-1';
}
var Tag = {
    /**
     * Insert un nouveau tag
     * @returns {undefined}
     */
    insert: function () {
        // tag code can not be empty
        if ( $('tag_code').value.trim()=='') {
            smoke.alert('Vous devez donner un code');
            return;
        }
        waiting_box();
        // compose the query string
        var query_string = $('tag_display_box_frm').serialize(true);
        query_string['do'] = 'ajax_tag_insert';
        
        // send to ajax
        new Ajax.Request('index.php', {
            method: 'get',
            parameters: query_string,
            onSuccess: function (p_result) {
                try 
                {
                    var xml = p_result.responseXML;
                    var answer=getValue(xml,'result');
                    remove_waiting_box();
                    // check the status
                    if ( answer == 'OK') {
                        warning('Ajouté '+$('tag_code').value.trim());
                        var row=document.createElement('tr');
                        row.id=getValue(xml,'id');
                        var display=getValue(xml,'display')
                        row.innerHTML=display;
                        display.evalScripts();
                        $('tag_display_table').appendChild(row);
                        Tag.display(-1);
                        $('tag_code').value="";
                        $('tag_code').focus();
                        $('tag_description').value="";
                        new Effect.Highlight(row.id);
                    } else {
                        var display=getValue(xml,'display');
                        smoke.alert(display);
                    } 
                } catch (e) {
                    console.warn(e.message);
                }
            }
        })
    },
    /**
     * Sauve un tag existant -> update
     */
    save: function () {
         if ( $('tag_code').value.trim()=='') {
            smoke.alert('Vous devez donner un code');
            return;
        }
         // compose the query string
        var query_string = $('tag_display_box_frm').serialize(true);
        query_string['do'] = 'ajax_tag_save';
        waiting_box();
        // send to ajax
        new Ajax.Request('index.php', {
            method: 'get',
            parameters: query_string,
            onSuccess: function (p_result) {
                try 
                {
                    remove_waiting_box();
                    var xml = p_result.responseXML;
                    var answer=getValue(xml,'result');
                    // check the status
                    if ( answer == 'OK') {
                        warning('Sauvé');
                        var p_tag_id = getValue(xml,"id");
                        var display=getValue(xml,'display');
                        document.getElementById('tag_id'+p_tag_id).innerHTML = display;
                        new Effect.Highlight('tag_id'+p_tag_id);
                        display.evalScripts();
                        removeDiv('tag_display_box');
                    } else {
                        var display=getValue(xml,'display');
                        smoke.alert(display);
                    } 
                } catch (e) {
                    console.warn(e.message);
                }
            }
        });
    },
    /** 
     * if p_tag_id is -1 then display an empty form otherwise , detail of tag
     * \param p_tag_id -1 or existing tag_id 
     */
    display: function (p_tag_id) {
        waiting_box();
        new Ajax.Request('index.php', {
            method: 'get',
            parameters: {do: 'ajax_tag_display', tag_id: p_tag_id},
            onSuccess: function (p_result) {
                remove_waiting_box();
                if (!$('tag_display_box')) {
                    var t = document.createElement('div');
                    t.id = 'tag_display_box';
                    t.addClassName('inner_box');
                    document.body.appendChild(t);
                }
                var top=150;
                new Effect.Appear('tag_display_box');
                t.style="top:"+top+"px;position:fixed";
                $('tag_display_box').innerHTML = p_result.responseText;
                $('tag_code').focus();
            }

        })
    },
    /**
     * Efface un tag
     * @returns {undefined}
     */
    delete: function (p_tag_id) {
        smoke.confirm ('Vous confirmez',function (e) {
            if ( !e ) return false;
            waiting_box();
            
            new Ajax.Request('index.php',
            {
                method:"get",
                parameters:{do:'ajax_tag_delete',tag_id:p_tag_id},
                onSuccess : function (xml) {
                    remove_waiting_box();
                    $('tag_id'+p_tag_id).hide();
                    warning('Effacé');
                }
            });
        });
    },
    /**
     * display a list of existing tags , the user choose one , so a hidden field
     * is updated and all the dossier / template selected will have this tag
     * The type is either d or m , with m for template , only the unused tag of template 
     * will be show, with d all the tags are shown
     * \param p_type d or m
     * \param p_mod_id mandatory for Modele
     */
    select_tag: function (p_type,p_mod_id) {
        waiting_box();
        // Display list of existing tag
        new Ajax.Request('index.php',{
            method:'get',
            parameters:{do:'ajax_tag_select','type':p_type,mod_id:p_mod_id},
            onSuccess : function (req) {
                remove_waiting_box();
                // display list of tag
                add_div ({id:'select_tag_box',cssclass:'inner_box',html:req.responseText,'style':'position:fixed;top:150px'});
                
            }
        })
    },
    /**
     * Set the tag or drop to a folder or a template, the tag value or drop
     * is the tag_selected elt. 
     * @param {type} p_type d for dossier m for template
     * @param {type} p_id dos_id or mod_id or modele_tag.id
     * @param p_action is set (for update) add (to add) del (for delete), for d it is always set
     * @param p_mod_tag_id is the modele_tag.id usefull for del and set
     * @returns update the row
     * 
     */
    set : function (p_type,p_id,p_action,p_mod_tag_id) {
        waiting_box();
        var send=p_mod_tag_id;
        if ( p_type == 'd' ) {
            remove_waiting_box();
            send = $('tag_selected').value;
            if ( send == -1 ) {
                smoke.alert('Choisissez une étiquette svp');
                return;
            }
        }
        new Ajax.Request('index.php',
        {
            method:'get',
            parameters:{do:'ajax_tag_set','p_id':p_id,'tag':send,type:p_type,action:p_action,mod_tag_id:p_mod_tag_id},
            onSuccess: function (req) {
                remove_waiting_box();
                try {
                    warning (" Sauvé ");
                    $('tcode'+p_id).innerHTML=req.responseText;
                    new Effect.Highlight('dos'+p_id);
                } catch(e){
                    smoke.alert(e.message);
                }
            }
        })
    }
    
};
var Teacher = {
    /**
     * save the selected tag in databa
     * @returns {undefined}
     */
    tag_add : function (p_use_id,p_tag_id) {
        waiting_box();
        new Ajax.Request('index.php',{
            method:'get',
            parameters:{do:'teacher_tag_add',tag_id:p_tag_id,use_id:p_use_id},
            onSuccess : function (req) {
                var xml=req.responseXML;
                //----------------------------------------------------
                //From XML , retrieve the new id of selected ,the result
                // of the operation and the tag_code to display
                //----------------------------------------------------
                var id=getValue(xml,'id');
                var status=getValue(xml,'status');
                var display=getValue(xml,'display');
                var code=getValue(xml,'code');
                warning(' ajout '+code);
                
                remove_waiting_box();
                removeDiv('display_tag_available_id');
                var elt=document.createElement('li');
                elt.id='tt'+id;
                elt.innerHTML=display;
                $("list"+p_use_id).appendChild(elt);
                new Effect.Highlight(elt.id);
            }
        });
    },
    /**
     * Display a list of available tag to add
     * \param {type} p_use_id
     */
    tag_select : function (p_use_id) {
        waiting_box();
        new Ajax.Request('index.php',{
            method:'get',
            parameters:{'do':'teacher_tag_select',use_id:p_use_id},
            onSuccess: function (req) {
                remove_waiting_box();
                add_div({id:'display_tag_available_id',html:req.responseText,cssclass:'inner_box','style':'position:absolute;top:150px'});
            }
        });
    },
    /**
     * remove a tag
     * \param p_teacher_tag_id
     */
    tag_remove:function (p_teacher_tag_id) {
        smoke.confirm('Confirmer effacement',function (e) {
            if (e) {
                waiting_box();
                new Ajax.Request('index.php',{
                    method:'get',
                    parameters:{'do':'teacher_tag_remove',teacher_tag_id:p_teacher_tag_id},
                    onSuccess: function (req) {
                        remove_waiting_box();
                        warning('Effacé');
                        new Effect.Fade($('tt'+p_teacher_tag_id));
                    }
                });
            }
        });
    },
    /**
     * @brief display a form to add a new teacher
     * @type type
     */
    new_teacher:function () 
    {
       waiting_box();
       
       new Ajax.Request('index.php',{
           method:'get',
           parameters:{do:'ajax_new_teacher'},
           onSuccess:function(req) {
               remove_waiting_box();
               var div_new = new Element('div',{
                   'class':'inner_box',
                   'id':'div_new_teacher',
                   'style':"height:361px;position:fixed;top:150px;right:50%"
                   
               });
               $(document.body).insert(div_new);
                new Effect.Appear('div_new_teacher');
               div_new.update(req.responseText);
               
           }
       })
       
    },
    /**
     * @brief save the new teacher
     * @type type
     */
    add_teacher:function() {
       // ajax_add_teacher
       try {
           waiting_box();
           var form=$F("input_new_teacher");
            new Ajax.Request('index.php'
                    ,{
                        method:'post'
                        ,parameters:{do:"ajax_add_teacher","teacher_login":form}
                        ,onSuccess:function(req) {
                            // Parse the XML and retriveve the status
                             var xml=req.responseXML;
            
                            var status=getNodeText(xml.getElementsByTagName("status")[0]);
                            var display=getNodeText(xml.getElementsByTagName("display")[0]);
                            var id_value=getNodeText(xml.getElementsByTagName("id")[0]);
                            if ( status =="OK" ) 
                            {
                                
                                var span_new=new Element("span",{"id":"teacher"+id_value});
                                $('teacher_list').appendChild(span_new);
                                span_new.update(display);
                                $("div_new_teacher").remove();
                            } else {
                                $('input_new_teacher_info').update(display);
                                $('input_new_teacher_info').className="error";
                                smoke.alert(display);
                            }
                        }
                    });
       }catch (e) {
           alert_box(e.message);
       }
       remove_waiting_box();
       return false;
    },
    /**
     * delete a teacher from this application
     * and remove right
     */
    remove_teacher: function (teacher_id, html_elt_id)
    {
        smoke.confirm('Confirmer effacement', function (e) {
            if (e) {
                waiting_box();
                new Ajax.Request('index.php', {
                    method: 'get'
                    , parameters: {'do': 'ajax_remove_teacher', 'teacher_id': teacher_id}
                    , onSuccess: function (req) {
                        if (req.responseText == "OK") {
                            $("teacher" + html_elt_id).remove();
                        } else {
                            alert_box(req.responseText);
                        }
                        remove_waiting_box();
                    }

                });
            }
        });
    }

    
};

var Modele= {
  drop:function(p_mod_id) {
      Tag.set('m',p_mod_id,'set','drop');
  }  
};
/**
 * Create a div without showing it
 * @param {type} obj
 *  the attributes are
 *   - style to add style
 *   - id to add an id
 *   - cssclass to add a class
 *   - html is the content
 *   - drag is the div can be moved
 * @returns html dom element
 * @see add_div
 */
function create_div(obj)
{
    try
    {
        var top = document;
        var elt = null;
        if (!$(obj.id)) {
            elt = top.createElement('div');
        } else {
            elt = $(obj.id);
        }
        if (obj.id)
        {
            elt.setAttribute('id', obj.id);
        }
        if (obj.style)
        {
            if (elt.style.setAttribute)
            { /* IE7 bug */
                elt.style.setAttribute('cssText', obj.style);
            } else
            { /* good Browser */
                elt.setAttribute('style', obj.style);
            }
        }
        if (obj.cssclass)
        {
            elt.setAttribute('class', obj.cssclass); /* FF */
            elt.setAttribute('className', obj.cssclass); /* IE */
        }
        if (obj.html)
        {
            elt.innerHTML = obj.html;
        }

        var bottom_div = document.body;
        elt.hide();
        bottom_div.appendChild(elt);

        /* if ( obj.effect && obj.effect != 'none' ) { Effect.Grow(obj.id,{direction:'top-right',duration:0.1}); }
         else if ( ! obj.effect ){ Effect.Grow(obj.id,{direction:'top-right',duration:0.1}); }*/
        if (obj.drag)
        {
            new Draggable(obj.id, {starteffect: function ()
                {
                    new Effect.Highlight(obj.id, {scroll: window, queue: 'end'});
                }}
            );
        }
        return elt;
    } catch (e)
    {
        error_message("create_div " + e.message);
    }
}
/**
 *@brief add dynamically a object for AJAX
 *@param obj.
 * the attributes are
 *   - style to add style
 *   - id to add an id
 *   - cssclass to add a class
 *   - html is the content
 *   - drag is the div can be moved
 */
function add_div(obj)
{
    try {
        var elt = create_div(obj);
        /* elt.setStyle({visibility:'visible'}); */
        elt.style.visibility = 'visible';
        elt.show();
    } catch (e)
    {
        alert_box("add_div " + e.message);
    }
}
/**
 * remove a object created with add_div
 * @param elt id of the elt
 */
function removeDiv(elt)
{
    if (document.getElementById(elt))
    {
        document.body.removeChild($(elt));
    }
}
function waiting_node()
{
    $('info_div').innerHTML = 'Un instant';
    $('info_div').style.display = "block";
}
/**
 *show a box while loading
 *must be remove when ajax is successfull
 * the id is wait_box
 */
function waiting_box()
{
    var obj = {
        id: 'wait_box', html: loading()
    };
    var y = fixed_position(10, 250)
    obj.style = y + ";width:20%;margin-left:40%;";
    if ($('wait_box')) {
        removeDiv('wait_box');
    }
    waiting_node();
    add_div(obj);
//    $('wait_box').setOpacity(0.7);

}
function remove_waiting_box()
{
    $('info_div').style.display = "none";
    $('info_div').innerHTML = '';
    removeDiv('wait_box');
}
function loading()
{
    var str = '<h2> Un instant ...</h2>';
    str = str + '<image src="image/loading.gif" alt="chargement"></image>';
    return str;
}
/**
 *@brief Firefox splits the XML into 4K chunk, so to retrieve everything we need
 * to get the different parts thanks textContent
 *@param xmlNode a node (result of var data = =answer.getElementsByTagName('code'))
 *@return all the content of the XML node
 */
function getNodeText(xmlNode)
{
    if (!xmlNode)
        return '';
    if (typeof (xmlNode.textContent) != "undefined")
    {
        return xmlNode.textContent;
    }
    if (xmlNode.firstChild && xmlNode.firstChild.nodeValue)
        return xmlNode.firstChild.nodeValue;
    return "";
}
/**
 * @brief compute the string to position a div in a fixed way
 * @return string
 */
function fixed_position(p_sx, p_sy)
{
    var sx = p_sx;
    var sy = calcy(p_sy);

    var str_style = "top:" + sy + "px;left:" + sx + "px;position:absolute";
    return str_style;

}
function getPosition(e)
{
    if (document.all)
    {
        posX=event.x+document.body.scrollLeft;
        posY=event.y+document.body.scrollTop;
    }
    else
    {
        posX=e.pageX;
        posY=e.pageY;
    }
}

/**
 *@brief compute Y even if the windows has scrolled down or up
 *@return the correct Y position
 */
function calcy(p_sy)
{
    var sy = p_sy;
    if (window.scrollY)
    {
        sy = window.scrollY + p_sy;
    }
    else
    {
        sy = document.body.scrollTop + p_sy;
    }
    return sy;

}
/**
 * Create a div which can be used in a anchor
 * @returns {undefined}
 */
function create_anchor_up()
{
    if ( $('up_top')) return;
    
    var newElt = new Element('div');
    newElt.setAttribute('id', 'up_top');
    newElt.innerHTML='<a id="up_top"></a>';
    
    var parent = $('info_div').parentNode;
    parent.insertBefore(newElt, $('info_div'));
    
}
/**
 * Initialize the window to show the button "UP" if the window is scrolled
 * vertically
 * @returns {undefined}
 */
function init_scroll()
{
    var up=new Element('div',{"class":"inner_box",
            "style":"padding:10px;left:auto;width:30px;height: auto;display:none;position:fixed;top:25px;right:20px;text-align:center",
            id:"go_up"
        });
        up.innerHTML=' <a class="button" href="#up_top" >&#8679;</a>';
        document.body.appendChild(up);
         window.onscroll=function () {
         if ( document.viewport.getScrollOffsets().top> 0) {
             if ($('go_up').visible() == false) {
                $('go_up').setOpacity(0.85); 
                $('go_up').show();
            }
        } else {
            $('go_up').hide();
        }
     }
}
/**
 * All the onload must be here otherwise the other will overwritten
 * @returns {undefined}
 */
window.onload=function ()
{
    create_anchor_up();
    init_scroll();
}

/**
 * @brief filter quickly a table
 * @param  phrase : phrase to seach
 * @param  _id : id of the table
 * @param  colnr : string containing the column number where you're searching separated by a comma
 * @param start_row : first row (1 if you have table header)
 * @returns nothing
 * @see HtmlInput::filter_table
 */
function filter_table(phrase, _id, colnr, start_row) {
    $('info_div').innerHTML = "Un instant";
    $('info_div').style.display = "block";
    var words = $(phrase).value.toLowerCase();
    var table = document.getElementById(_id);

    // if colnr contains a comma then check several columns
    var aCol = new Array();
    if (colnr.indexOf(',') >= 0) {
        aCol = colnr.split(',');
    } else {
        aCol[0] = colnr;
    }
    var ele;
    var tot_found = 0;

    for (var r = start_row; r < table.rows.length; r++) {
        var found = 0;
        for (var col = 0; col < aCol.length; col++)
        {
            var idx = aCol[col];
            if (table.rows[r].cells[idx])
            {
                ele = table.rows[r].cells[idx].innerHTML.replace(/<[^>]+>/g, "");
                //var displayStyle = 'none';
                if (ele.toLowerCase().indexOf(words) >= 0) {
                    found = 1;
                }
            }

        }
        if (found === 1) {
            tot_found++;
            table.rows[r].style.display = '';
        } else {
            table.rows[r].style.display = 'none';
        }
        $('info_div').style.display = "none";
        $('info_div').innerHTML = "";
    }
    if (tot_found == 0) {
        if ($('info_' + _id)) {
            $('info_' + _id).innerHTML = " Aucun résultat ";
        }
    } else {
        if ($('info_' + _id)) {
            $('info_' + _id).innerHTML = "  ";
        }
    }
}

var Database = {
    /**
     * Confirm DB destruction , display a list of database to drop
     */
    confirm:function () 
    {
        waiting_box();
        new Ajax.Request('index.php',{
            method:'get',
            parameters:{do:'ajax_drop_confirm'},
            onSuccess: function(req){
                try {
                    remove_waiting_box();
                    add_div({id:'confirm_drop_div',
                        cssclass:'inner_box',
                        style:'position:absolute;top:2px;left:0px;width:99%;height:100%',
                        html:req.responseText});
                }catch(e) {
                    smoke.alert(e.message);
                }
              }
            });
    }
    ,
    /**
     * Drop the databases
     *
     */
    dropthem: function () {
        smoke.confirm('Vous confirmez ?', function (e) {
            if (e) {
                waiting_box();
                new Ajax.Request('index.php',
                        {
                            method: 'get',
                            parameters: {'do': 'ajax_drop'},
                            onSuccess: function (req) {
                                remove_waiting_box();
                                $('confirm_drop_div').innerHTML = req.responseText;
                            }
                        });
            }
        });

    }
};

function getSelectionId(text, li) {
    alert (li.id);
    alert (text);
}