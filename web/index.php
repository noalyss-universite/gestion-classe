<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

// Copyright Author Dany De Bontridder danydb@aevalys.eu


require_once __DIR__.'/../gestion_constant.php';
require_once NOALYSS_INCLUDE.'/constant.php';
require_once NOALYSS_INCLUDE.'/lib/html_input.class.php';
require_once NOALYSS_INCLUDE.'/lib/ac_common.php';
require_once GESTION.'/lib/function_gestion.php';


define ('ALLOWED',true);

$cn=new Database();

if ( $cn->exist_schema('extended_admin')  == false ) {
    install_extended_admin($cn);
}
// get the db version
$dbversion=$cn->get_value("select max(id) from extended_admin.version");

// if version == 1 , then upgrade and execute the patch 1
if ( $dbversion == 1 ) {
  // Execute the SQL Patch 
  $cn->execute_script(GESTION.'/sql/patch1.sql');
  // If a XML is present 
  if ( file(GESTION.'/teachers.xml')) { 
    // Read_xml and save it into the db
    require_once GESTION.'/class/class_gestion_teacher.php';
    $teacher=new Gestion_Teacher($cn);
    $teacher->migrate_from_xml();
  }
  $cn->execute_script(GESTION.'/sql/patch2.sql');
}

$action = HtmlInput::default_value_request('do','xxx');


$a_key=array_keys($a_path);

if (in_array($action, $a_key)) {
     require GESTION.'/include/'.$a_path[$action]['file'];
     return;
} 
$a_key=array_keys($a_ajax);
if ( in_array($action,$a_key) ) {
     require GESTION.'/ajax/'.$a_ajax[$action];
     return;
}
html_start();
display_gestion_menu("");
