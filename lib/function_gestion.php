<?php


function html_start() 
{
    
    echo 
    '<!doctype html>',
    '<HTML><HEAD>',
    '<TITLE>',
    'Université  Gestion ',
    '</TITLE>',
    '<link rel="icon" type="image/ico" href="favicon.ico" />',
    '<META http-equiv="Content-Type" content="text/html; charset=UTF-8">',
    '<meta name="viewport" content="width=device-width, initial-scale=1.0">',
    '<LINK REL="stylesheet" type="text/css" href="style-screen.css?version='.SVNINFO.'" media="screen"/>',
    '<link rel="stylesheet" type="text/css" href="./style-print.css?version='.SVNINFO.'" media="print"/>',
    '<script type="text/javascript" charset="utf-8" language="javascript" src="js/prototype.js">',
    '</script>',
    '<script type="text/javascript" charset="utf-8" language="javascript" src="js/scriptaculous.js"></script>',
    '<script type="text/javascript" charset="utf-8" language="javascript" src="js/smoke.js">',
    '</script>',
    '<script type="text/javascript" charset="utf-8" language="javascript" src="js/sorttable.js">',
    '</script>',
    '<script type="text/javascript" charset="utf-8" language="javascript" src="js/extended_admin_script.js">',
    '</script>',
    '</head>',
    '<body>';
    echo '<div id="info_div"></div>';
    echo '<div id="warning_div"></div>';
    echo '<h1>',
         ECOLE,
         '</h1>';
    
}

function display_gestion_menu($p_selected)
{
    global $a_path;
    echo '<ul class="tabs">';
    $nb_item = count($a_path);
    $key=array_keys($a_path);
    for ($i=0;$i<$nb_item;$i++) {
        $x=$key[$i];
        $class=($p_selected==$x)?' tabs_selected ':'';
        echo '<li ','class="',
                $class,
                '">',
                '<a href="?do=',$key[$i],'">',
                $a_path[$x]['label'],
                '</a>',
                '</li>';
    }
    echo '</ul>';
}

function install_extended_admin($p_cn) 
{
    $p_cn->execute_script(GESTION.'/sql/init.sql');
}