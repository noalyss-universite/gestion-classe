<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))     die('Appel direct ne sont pas permis');
/**
 * @file
 * @brief Tag Management
 * 
 */
html_start();

// Show list and form
display_gestion_menu('log');

$array=$cn->get_array( " SELECT 
    id, 
    ins_ip_addr, 
    ins_date_subscribe, 
    ins_email, 
    ins_name, 
    ins_first_name, 
    ins_tag, 
    ins_date_last_result, 
    ins_result_process, 
    ins_flag_result, 
    use_id, 
    mod_id, 
    dos_id
FROM extended_admin.inscription
        order by id desc limit 200

    ");
$a_flag=array('S'=>'Succès','E'=>'Erreur','P'=>'En-cours');
$nb_array=count($array);
?>
<table style="width: auto">
    
    <?php for ($i=0;$i<$nb_array;$i++):?>
    <tr>
        <td> <?php echo h($array[$i]['ins_ip_addr'])?></td>
        <td> <?php echo h($array[$i]['ins_date_subscribe'])?></td>
        <td> <?php echo h($array[$i]['ins_email'])?></td>
        <td> <?php echo h($array[$i]['ins_name'])?></td>
        <td> <?php echo h($array[$i]['ins_first_name'])?></td>
        <td> <?php echo h($array[$i]['ins_flag_result'])?></td>
        <td> <?php echo h($array[$i]['ins_result_process'])?></td>


    </tr>
    <?php endfor;?>
    
</table>
