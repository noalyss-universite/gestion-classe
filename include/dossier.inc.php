<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))     die('Appel direct ne sont pas permis');
require_once GESTION.'/class/class_gestion_dossier.php';
/**
 * @file
 * @brief Tag Management
 */
html_start();

// Show list and form
display_gestion_menu('dossier');
$cn=new Database();

$x=new Gestion_Dossier($cn);
?>
<ul class="button_choice">
    <li>
        <input type="button" class="smallbutton" value="Choix Etiquette" onclick="Tag.select_tag('d');">
    </li>
    <li>
        <input type="button" class="smallbutton" value="Marquage pour effacement" onclick="$('tag_selected').value='drop';$('tag_code_selected').innerHTML='EFFACEMENT A VALIDER';$('tag_code_selected').addClassName('notice');$('tag_code_selected').show();">
    </li>
    <li>
        <input type="button" class="smallbutton" value="Valider effacement" onclick="Database.confirm()">
    </li>
</ul>
<input type="hidden" id="tag_selected" value="-1">
<span id="tag_code_selected"></span>
<script>$('tag_code_selected').hide();</script>
<?php
$x->display_dossier();