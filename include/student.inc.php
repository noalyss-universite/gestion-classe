<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

/**
 * @file 
 * The purpose of this file is to manage the students : display a list ,
 * remove or inactivate some of them
 */
if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

html_start();
display_gestion_menu('student');
require_once NOALYSS_INCLUDE.'/class/dossier.class.php';

$repo=new Dossier(0);
/*
 * If remove is asked (and  confirmed) then remove
 */
if (isset($_POST['action']))
{
// detect action
    $action=HtmlInput::default_value_post("action", 0);


    $a_student=HtmlInput::default_value_post("student", "");

    if (is_array($a_student)&&count($a_student)>0)
    {
        $nb_student=count($a_student);
        for ($i=0; $i<$nb_student; $i++)
        {
            // protect NOALYSS_ADMINISTRATOR
            if ($a_student[$i]==1)
                continue;

            // action == Activate selected students
            if ($action=="1")
            {

                $cn->exec_sql("update ac_users set use_active = 1 
                        where
                        use_id=$1 ", array($a_student[$i]));
            }
            // Deactivate selected students
            else if ($action=="2")
            {
                $cn->exec_sql("update ac_users set use_active = 0
                        where
                        use_id=$1 ", array($a_student[$i]));
            } // action == delete them
            else if ($action=="3")
            {
                $cn->exec_sql("delete from jnt_use_dos where use_id=$1",
                        array($a_student[$i]));
                $cn->exec_sql("delete from ac_users where use_id=$1",
                        array($a_student[$i]));
            }
        }
       
    }
}
/*
 * Show the list of students
 */

$a_user=$repo->get_user_folder(" order by use_login");
$max_select=ini_get("max_input_vars");
if ($max_select <> 0 ) printf('nombre maximum de choix <span id="left_student">%d</span>',$max_select);
?>
<script>
    function confirm_form() 
    {
        waiting_box();
        var param=$('student_frm').serialize();
        param+="&do=ajax_remove_student";
        new Ajax.Request("index.php",{
           method:"post",
           parameters:param,
           onSuccess:function (req)
           {
               if ( ! $('student_confirm') ) {
                    var div=new Element("div",{"id":"student_confirm",class:"inner_box"});
                    
                } else {
                    div=$('student_confirm');
                }
               var y=calcy(200);
               div.setStyle("position:absolute;top:"+y+"px;rigth:50%;width:40%");
               $(document.body).insert(div);
               div.update(req.responseText);
               new Draggable("student_confirm", {starteffect: function ()
                        {
                            new Effect.Highlight("student_confirm", 
                            {scroll: window, queue: 'end'});
                        }}
                    );
               remove_waiting_box();
           }
         
        });
        
        return false;
    }
</script>

<FORM id="student_frm" METHOD="POST" onsubmit="return confirm_form()">


    <?php
    if (!empty($a_user))
    {
        echo '<span style="display:block">';
        echo _('Cherche');
        echo HtmlInput::filter_table("user", "1,2,5", "1");
        echo '</span>';
        echo '<table id="user" class="sortable">';
        echo '<tr>';
        echo td("");
        echo '<th>'."Login".'</th>';
        echo '<th>'."nom".'</th>';
        echo th("Prénom");
        echo '<th>'."Actif".'</th>';
        echo "<th>".'Type'."</th>";
        echo '<th>'."Dossier".'</th>';
        echo '</tr>';
        $compteur=0;
        foreach ($a_user as $r_user)
        {
            $compteur++;
            $class=($compteur%2==0)?"odd":"even";
            $checkbox=new ICheckBox("student[]");
            $checkbox->value=$r_user['use_id'];
            echo "<tr class=\"$class\">";
            if ($r_user['use_active']==0)
            {
                $Active=$g_failed;
            }
            else
            {
                $Active=$g_succeed;
            }
            echo "<td>",
            $checkbox->input(),
            "</td>";

            echo td($r_user['use_login']);

            echo td($r_user['use_name']);
            echo td($r_user['use_first_name']);
            echo td($Active);
            $type=($r_user['use_admin']==1)?"Administrateur":"Utilisateur";
            echo "<td>".$type."</td>";
            if ($r_user['use_admin']==0)
                echo td($r_user['ag_dossier']);
            else
            {
                echo td(_('Tous'));
            }
            echo '</tr>';
        }// foreach
        echo '</table>';
    } // 
    $action=new ISelect("action");
    $action->value=array(
        array("label"=>"--", "value"=>0),
        array("label"=>"Activer", "value"=>1),
        array("label"=>"Désactiver", "value"=>2),
        array("label"=>"Effacer", "value"=>3),
            )
    ?>
    <p>
        Action pour les utilisateurs choisis , 
<?php echo $action->input() ?>
    </p>
    <ul class="button_choice">
        <li>
            <?php echo HtmlInput::submit("validate", "Valider") ?>
        </li>
    </ul>
</FORM>

