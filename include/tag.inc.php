<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))     die('Appel direct ne sont pas permis');
require_once GESTION.'/class/class_gestion_tag.php';
/**
 * @file
 * @brief Tag Management
 * 
 */
html_start();
display_gestion_menu('tag');
$cn=new Database();

$x=new Gestion_Tag($cn);
echo '<input type="button" class="smallbutton" value="Ajout étiquette" onclick="Tag.display(-1)">';
$x->display_list();
echo '<input type="button" class="smallbutton" value="Ajout étiquette" onclick="Tag.display(-1)">';
?>
