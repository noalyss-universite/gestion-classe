<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
require_once GESTION.'/database/class_extended_admin_dossier_tag_sql.php';
require_once NOALYSS_INCLUDE.'/class/database.class.php';

/**
 * @file
 * @brief 
 * @param type $name Descriptionara
 */
class Gestion_Dossier 
{
    private $cn; //!< Database connection
    private $data; //!< Extended_Admin_Dossier_Tag object
    
    function __construct($p_cn,$p_id=-1)
    {
        $this->data=new Extended_Admin_Dossier_tag_SQL($p_cn,$p_id);
        $this->cn=$p_cn;
    }
    function display_dossier()
    {
        $sql = " 
            select 
        	dtag.id as dossier_tag_id,ac_dossier.dos_id, 
                dtag.dt_drop as dt_drop,
                ac_dossier.dos_name,
                ac_dossier.dos_description,
                tag.id as tag_id, tag_code
            from 
            ac_dossier 
            left join extended_admin.dossier_tag as dtag on (ac_dossier.dos_id=dtag.dos_id)
            left join extended_admin.tag on (tag.id=tag_id)";
        
        require GESTION.'/template/gestion_dossier_display_dossier.php';


    }
    function display_row($p_array) {
        static $x=0;
         $content=(h($p_array['tag_code'])!='')?h($p_array['tag_code']):'aucune';
         $content = ( $p_array['dt_drop'] == 'Y') ? "Effacement à valider":$content;
         $class=($x%2==0)?' class="even"':'';
        $x++;
        ?>
    <tr id="dos<?php echo $p_array['dos_id']?>" <?php echo $class?> >
        <td>
            <?php echo h($p_array['dos_name']) ?>
        </td>
        <td>
            <?php echo h($p_array['dos_description']) ?>
        </td>
        <td>
            <a href="javascript:void(0)" id="tcode<?php echo $p_array['dos_id']?>" onclick="Tag.set('d',<?php echo $p_array['dos_id']?>,'set');return false;">
            <?php echo $content;?>
            </a>
        </td>
     
    </tr>
    <?php
    }
    function set_folder($p_dos_id)     
    {
        $id=$this->cn->get_value ('select id from extended_admin.dossier_tag where dos_id=$1',
                array($p_dos_id));
        if ( $id==''){
            $id=-1;
        }
        $this->data->id=$id;
        $this->data->load();
        $this->data->dos_id=$p_dos_id;
        
    }
    /**
     * Remove a teacher and all his privileges
     * So it cannot connect anymore and all his privileges are removed from
     * the dossier
     * 
     */
    function remove_teacher() {
        // Retrieve the teacherS allowed to access this tag
        $a_teacher=$this->cn->get_array('select use_id from extended_admin.teacher_tag where tag_id=$1',
                array($this->data->tag_id));
        $nb_teacher=count($a_teacher);
        
        for ($i=0;$i<$nb_teacher;$i++) 
        {
            // remove him from jnt_use_dos
            $this->cn->exec_sql("delete from jnt_use_dos WHERE use_id=$1 and dos_id=$2",
                array($a_teacher[$i]['use_id'],$this->data->dos_id));
            // remove him to db
            $teacher_login=$this->cn->get_value('select use_login from public.ac_users where use_id=$1',
                    array($a_teacher[$i]['use_id']));
            
            // Remove teacher from user's dossier
            $cn_dossier=new Database($this->data->dos_id);
            if ( $teacher_login != '') {
                $cn_dossier->exec_sql("delete from profile_user where user_name=$1",array($teacher_login));
                $cn_dossier->exec_sql("delete from user_sec_act where ua_login=$1",array($teacher_login));
                $cn_dossier->exec_sql("delete from user_sec_jrn where uj_login=$1",array($teacher_login));
            }

        }
            
    }
    /**
     * Update the description of a folder depending on the tag
     */
    function update_description($p_clear = false) {
        // Update the description of the folder
        $email = $this->cn->get_value (" select ins_email from extended_admin.inscription where dos_id=$1",
                array($this->data->dos_id));
        
        $tag_code="";
        if ( ! $p_clear)
        {
            $tag_code = $this->cn->get_value(" select tag_code from extended_admin.tag where id=$1",
                array($this->data->tag_id));
        } 
        $description = $email . " ".$tag_code;
        
        $this->cn->exec_sql("update ac_dossier set dos_description = $1 where dos_id=$2",
                array($description,$this->data->dos_id));

    }
    /**
     * Grant Teacher access to folder, grant administrator profile , all the ledgers and all the action
     * 
     */
    function grant_teacher() 
    {
        $a_teacher=$this->cn->get_array("select use_id,use_login
                                from extended_admin.teacher_tag
                                join ac_users using (use_id)
                                where tag_id=$1", array($this->data->tag_id));
        $nb_teacher=count($a_teacher);
        if ( $this->data->dos_id )
        $cn_dossier=new Database($this->data->dos_id);
        for ($i=0; $i<$nb_teacher; $i++)
        {
            if ( 
                    $this->cn->get_value("select count(*) from jnt_use_dos where use_id=$1 and dos_id=$2",
                            array($a_teacher[$i]['use_id'],$this->data->dos_id)) == 0
                    )
            {
                $this->cn->exec_sql("insert into jnt_use_dos(use_id,dos_id) values ($1,$2)",
                    array($a_teacher[$i]['use_id'], $this->data->dos_id));
            }
            if ( $cn_dossier->get_value("select count(*) from profile_user where user_name=$1",
                    array($a_teacher[$i]['use_login'])) == 0)
            {
                $cn_dossier->exec_sql('insert into profile_user(user_name,p_id) values($1,1)',
                        array($a_teacher[$i]['use_login']));
            }
            // Grant all action + ledger to him
            $cn_dossier->exec_sql("delete from user_sec_act where ua_login=$1",array($a_teacher[$i]['use_login']));
            
            $cn_dossier->exec_sql("insert into user_sec_act (ua_login,ua_act_id)"
                    ." select $1 ,ac_id from action ",array($a_teacher[$i]['use_login']));
            
            $cn_dossier->exec_sql("delete from user_sec_jrn where uj_login=$1",array($a_teacher[$i]['use_login']));
            $cn_dossier->exec_sql("insert into user_sec_jrn(uj_login,uj_jrn_id,uj_priv)"
                    ." select $1,jrn_def_id,'W' from jrn_def",
                            array($a_teacher[$i]['use_login']));
        }
    }
    /**
     * When setting a tag , the security is updated
     * @param type $p_tag_id
     */
    function set_tag($p_tag_id) 
    {
        // remove old access
        $this->remove_teacher();
        
        $old_tag=$this->data->tag_id;
        $this->data->tag_id=$p_tag_id;
        $this->data->dt_drop='N';
        $this->data->save();
        
        $this->update_description();
        
        // set the permission on it to the right teacher depending on teacher_tag
        $this->grant_teacher();
        
        
        
        
    }
    function mark_drop(){
        if ( $this->data->dt_drop=='Y' ) {
            $this->data->dt_drop='N';
        } else {
            $this->data->dt_drop='Y';
        }
        $this->data->save();
    }
    function get_drop() {
        return $this->data->dt_drop;
    }
    function get_tag() {
        return $this->data->tag_id;
    }
}
?>
