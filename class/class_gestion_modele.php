<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
require_once GESTION.'/database/class_extended_admin_modele_tag_sql.php';

/**
 * @file
 * @brief 
 * @param type $name Descriptionara
 */
class Gestion_Modele
{
    private $cn; //!< Database connection
    private $data; //!< Extended_Admin_Dossier_Tag object
    
    function __construct($p_cn,$p_id=-1)
    {
        $this->data=new Extended_Admin_Modele_tag_SQL($p_cn,$p_id);
        $this->cn=$p_cn;
    }
    function get_array($p_mod_id) 
    {
        $sql = " 
          select distinct 
        	modeledef.mod_id, 
                dtag.mt_drop as mt_drop,
                modeledef.mod_name,
		modeledef.mod_desc
            from 
            modeledef 
            left join extended_admin.modele_tag as dtag on (modeledef.mod_id=dtag.mod_id)
            where 
            modeledef.mod_id=$1
            "; 
        $array=$this->cn->get_array($sql,array($p_mod_id));;
        if ( count($array) ==1) return $array[0];
        return null;
    }
    function display_modele()
    {
        $sql = " 
          select distinct 
        	modeledef.mod_id, 
                dtag.mt_drop as mt_drop,
                modeledef.mod_name,
		modeledef.mod_desc
            from 
            modeledef 
            left join extended_admin.modele_tag as dtag on (modeledef.mod_id=dtag.mod_id)
            
            ";
        
        require GESTION.'/template/gestion_modele_display_dossier.php';


    }
    function display_cell_tag($p_array)
    {
         $a_tag=$this->cn->get_array("
            select tag.id as tag_id, tag.tag_code , mod.id
            from
                extended_admin.modele_tag as mod 
                join extended_admin.tag on (tag.id=mod.tag_id)
                where 
                mod.mod_id=$1
            ",array($p_array['mod_id']));
        $nb_tag = count($a_tag);
        ?>
 <td id="tcode<?php echo $p_array['mod_id']?>" >
     <?php
     if ($p_array['mt_drop']=='Y') echo "Effacement à confirmer";
     else {
     ?>
            <ul class="tag_list">
                    
            <?php for ($i=0;$i<$nb_tag;$i++) { ?>
            <li>
            
            
            <?php echo $a_tag[$i]['tag_code'];?>
            <input type="button" class="tinybutton" value="X" onclick="Tag.set('m',<?php echo $p_array['mod_id']?>,'del',<?php echo $a_tag[$i]['id']?>)">
                
            </li>    
            <?php } ?>
            </ul>
            <input type="button" class="smallbutton"  value="Ajout"  onclick="Tag.select_tag('m',<?php echo $p_array['mod_id']?>,'add')">
     <?php }?>            
        </td>
<?php
    }
    function display_row($p_array) {
        if ($p_array == null) return;
        static $x=0;
         $content =  "Récupérer ou effacer ";
         $class=($x%2==0)?' class="even"':'';
        $x++;
       
        ?>
    <tr id="dos<?php echo $p_array['mod_id']?>"  <?php echo $class?>>
        <td>
            <?php echo h($p_array['mod_name']) ?>
        </td>
        <td>
            <?php echo h($p_array['mod_desc']) ?>
        </td>
        <?php $this->display_cell_tag($p_array);?>
        <td>
            <a class="smallbutton" onclick="Modele.drop(<?php echo $p_array['mod_id']?>,'drop');">
            <?php echo $content;?>
            </a>
        </td>
    </tr>
    <?php
    }
    function load() {
        $this->data->load();
    }
    function set_modele($p_id)     
    {
     
        $this->data->mod_id=$p_id;
        $this->data->load();
        
    }
    function set_id($p_id) {
        $this->data->id=$p_id;
        $this->data->load();
    }
    /**
     * Do not change security or description , only change the 
     * @param type $p_tag_id
     */
    function set_tag($p_tag_id) 
    {
        $this->data->tag_id=$p_tag_id;
        $this->save();
    }
    /**
     * \brief remove a tag from a template
     */
    function remove_tag() {
        $this->data->delete();
    }
    /**
     * \brief Add a tag to template
     */
    function add_tag ($p_tag) {
        $count_tag=$this->cn->get_value("select count(*) from extended_admin.modele_tag where tag_id=$1",
                array($p_tag));
        if ( $count_tag > 0) return false;
        $count=$this->cn->get_value ('select count(mt_drop) from extended_admin.modele_tag where mod_id=$1 and mt_drop=$2',
                array($this->data->mod_id,'Y'));
        $drop=($count > 0)?'Y':'N';
        
        $this->data->id=-1;
        $this->data->tag_id=$p_tag;
        $this->data->mt_drop=$drop;
        $this->data->insert();
        return true;
    }
    function mark_undrop(){
        $this->cn->exec_sql("update extended_admin.modele_tag set mt_drop = $1 where mod_id=$2",
                array('N',$this->data->mod_id));
        
    }
    function mark_drop(){

        $this->cn->exec_sql("update extended_admin.modele_tag set mt_drop = $1 where mod_id=$2",
                array('Y',$this->data->mod_id));
    }
    function get_drop() {
         $count=$this->cn->get_value ('select count(mt_drop) from extended_admin.modele_tag where mod_id=$1 and mt_drop=$2',
                array($this->data->mod_id,'Y'));
        $drop=($count > 0)?'Y':'N';
        if ($drop == 'Y') {
            $this->cn->exec_sql("update extended_admin.modele_tag set mt_drop = $1 where mod_id=$2",
                array('Y',$this->data->mod_id));
        } 
        return $drop;
    }
    function get_tag() {
        return $this->data->tag_id;
    }
    function get_mod_id() {
        return $this->data->mod_id;
    }
}
?>
