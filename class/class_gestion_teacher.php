<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');

require_once GESTION.'/database/class_extended_admin_teacher_tag_sql.php';
require_once GESTION.'/database/class_extended_admin_tag_sql.php';
require_once GESTION.'/class/class_gestion_dossier.php';

class Gestion_Teacher
{
    private $teacher_tag ; // teacher_tag_id teacher_tag.id
    private $a_teacher ; // array of teacher
            
    function __construct(Database $p_cn)
    {
        $this->cn=$p_cn;
        $this->read_table();
        $this->teacher_tag=new Extended_Admin_Teacher_Tag_SQL($this->cn);
    }
    /**
     * Display all the tags for each teacher
     */
    function display_teacher_tag()
    {
        $nb_teacher=count($this->a_teacher);
        for ($i=0; $i<$nb_teacher; $i++)
        {
            /*
             * Display tags for him if login exists
             */
            if ($this->a_teacher[$i]['use_id']!="")
            {
                echo '<span id="teacher'.$this->a_teacher[$i]['use_id'].'">';
                $this->display_teacher($this->a_teacher[$i]);
                echo "</span>";
            }

        }
    }
    /**
     * @brief display only one span for one teacher , the array has the index
     *  - use_id 
     *  - use_login
     * @param array $a_teacher [ use_id , use_login ]
     */
    function display_teacher($a_teacher)
    {
        
        echo '<h2>', $a_teacher['login'], '</h2>';
        $a_tag=$this->cn->get_array(
                ' select teacher_tag.id as teacher_tag_id,tag_id,tag_code 
                        from extended_admin.teacher_tag 
                        join extended_admin.tag on (tag.id=tag_id)
                        where use_id=$1 order by tag_code',
                array($a_teacher['use_id']));
        $nb_tag=count($a_tag);
        echo '<ul class="tag_list" id="list'.$a_teacher['use_id'].'">';
        for ($j=0; $j<$nb_tag; $j++)
        {
            $this->display_tag($a_tag[$j]);
        }
        echo '</ul>';
        // Display button to remove it 
        echo '<ul class="button_choice">';
        echo '<li>';
        echo HtmlInput::button_action('Enlever ce professeur',
                'Teacher.remove_teacher(\''.$a_teacher['use_id'].'\',\''.$a_teacher['use_id'].'\')',
                'x', 'smallbutton');
        echo '</li>';
        // button to add a tag
        echo '<li>';
        echo HtmlInput::button_action('Ajout étiquette',
                'Teacher.tag_select(\''.$a_teacher['use_id'].'\')',
                'x', 'smallbutton');
        echo '</li>';
        echo '</ul>';
    }

    /**
     * Display one tag
     * @param type $p_tag
     */
    function display_tag($p_tag) 
    {
        echo '<li id="tt'.$p_tag['teacher_tag_id'].'">',
             h($p_tag['tag_code']),
             HtmlInput::button_action('x',sprintf("Teacher.tag_remove('%s')",$p_tag['teacher_tag_id']),'x','tinybutton'),
             '</li>';
    }
    /**
     * @brief load the table extended_admin.teacher 
     */
    function read_table()
    {
        $this->a_teacher=$this->cn->get_array("select t.use_id as use_id,use_login as login 
            from 
            ac_users as au 
            join extended_admin.teacher as t on (au.use_id = t.use_id)");
    }
    /**
     * \@brief read xml file and fill the  array of teachers
     * echo a warning if the teacher is not found
     */
    function read_xml()
    {
        $dom=new DOMDocument('1.0');
        $dom->load(GESTION.'/teachers.xml');
        $xml=simplexml_import_dom($dom);
        if (!isset($xml->teacher))
            throw new Exception('Fichier invalide');

        $nb_teacher=count($xml->teacher);
        for ($i=0; $i<$nb_teacher; $i++)
        {
            $this->a_teacher[$i]['login']=(String) $xml->teacher[$i];
            $this->a_teacher[$i]['use_id']=$this->cn->get_value('select use_id 
                    from ac_users where use_login=$1',
                    array((String) $xml->teacher[$i]));
            // check if exists
            if ($this->a_teacher[$i]['use_id']=='')
            {
                echo "<br>Attention: utilisateur ", (String) $xml->teacher[$i], " n'existe pas ";
            }
        }
    }
    /**
     * Insert $this->ateacher into the table
     */
    function migrate_from_xml()
    {
        $this->read_xml();
        $nb_teacher=count($this->a_teacher);

        for ($i=0; $i<$nb_teacher; $i++)
        {
            // check if exists
            if ($this->a_teacher[$i]['use_id'] !='')
            {
                $this->cn->exec_sql("insert into 
                    extended_admin.teacher (use_id)values ($1)",
                        array($this->a_teacher[$i]['use_id']));
            }
            
            
        }
    }
    /**
     * Display Tag List available for the given user
     * \param $p_use_id is the id of the users (ac_users.use_id)
     */
    function show_available_tag($p_use_id) 
    {
        $sql="
            select tag_code,tag.id as tag_id ,tag_description
                from 
                extended_admin.tag
                where
                tag.id not in (select tag_id from extended_admin.teacher_tag where use_id=$1)
                order by tag_code
            ";
        $array=$this->cn->get_array($sql,array($p_use_id));
        require GESTION.'/template/gestion_teacher_available_tag.php';
        
    }
    function set_tag($p_id) {
        $this->teacher_tag->setp('tag_id',$p_id);
    }
    function set_teacher($p_id) {
        $this->teacher_tag->setp('use_id',$p_id);
    }
    /**
     * Add tag and update security
     */
    function add() {
        $teacher_tag=$this->teacher_tag->insert();
         $a_dossier=$this->cn->get_array("
             select dos_id
                from extended_admin.dossier_tag 
                join extended_admin.teacher_tag using (tag_id)
                  where teacher_tag.id=$1",
                array($this->teacher_tag->id));
        
        $nb_dossier=count($a_dossier);
        for ($i=0;$i<$nb_dossier;$i++)
        {
            $dossier=new Gestion_Dossier($this->cn);
            $dossier->set_folder($a_dossier[$i]['dos_id']);
            $dossier->grant_teacher();
        }
    }
    function get_tag_code() {
        $tag=new Extented_Admin_Tag_SQL($this->cn,$this->teacher_tag->tag_id);
        return $tag->getp('code');
    }
    function ajax_response_xml($p_status,$p_display) {
        $dom=new DOMDocument("1.0");
        $root=$dom->createElement("root");
        $id=$dom->createElement("id",$this->teacher_tag->id);
        $display=$dom->createElement("display",$p_display);
        $status=$dom->createElement("status",$p_status);
        $root->appendChild($id);
        $root->appendChild($status);
        $root->appendChild($display);
        $dom->appendChild($root);
        header('Content-type: text/xml; charset=UTF-8');
        echo $dom->saveXML();
    }
    function set_teacher_tag($p_id) {
        $this->teacher_tag->id=$p_id;
        $this->teacher_tag->load();
    }
    /**
     * remove tag  from teacher and update security
     */
    function remove() {
        $a_dossier=$this->cn->get_array("select dos_id from extended_admin.teacher_tag "
                . " join extended_admin.dossier_tag using (tag_id) "
                . " where teacher_tag.id=$1",
                array($this->teacher_tag->id));
        
        $nb_dossier=count($a_dossier);
        for ($i=0;$i<$nb_dossier;$i++)
        {
            $dossier=new Gestion_Dossier($this->cn);
             $dossier->set_folder($a_dossier[$i]['dos_id']);
            $dossier->remove_teacher();
        }
        $this->teacher_tag->delete();
    }
    function get_teacher_tag_id(){
        return $this->teacher_tag->id;
    }
    /**
     * @brief Display a form to enter a new teacher
     */
    function new_teacher()
    {
            echo h2("Nouveau professeur");
    ?>
<form method="get" id="form_add_teacher" onsubmit="Teacher.add_teacher();return false">
    <input class="input_text" type="text" id="input_new_teacher" name="input_new_teacher" autofocus style="width: 95%" >
    <div id="ind_input_new_teacher" class="autocomplete_fixed" style="position:static;height:290px;width:60%"> </div>
    <div>
        <span id="input_new_teacher_info"></span>
    <ul class="button_choice">
        <li>
        <?php   echo HtmlInput::submit("save_add_teacher","Sauver"); ?>
        </li>
        <li>
        <?php echo HtmlInput::button_close("div_new_teacher");?>
        </li>
    </ul>
    </div>
</form>
<script>
 $('input_new_teacher').focus();
 new Ajax.Autocompleter("input_new_teacher"
    ,"ind_input_new_teacher"
    ,"index.php?do=findTeacher"
    ,{
       paramName:"cteacher"
       ,minChars:1
    });
</script>
<?php
    }
    /**
     * @brief add a teacher in the SQL table extended_admin.teacher
     */
    function add_teacher($p_login)
    {
        if (trim($p_login) == "" ) 
        {
            $this->ajax_response_add_teacher ("NOK","Ne peut être vide");
            return;
        }
        // check if login exists
        $id=$this->cn->get_value("select use_id from ac_users 
                where upper(use_login) = upper(trim($1)) " , array($p_login));
        if ($id == "") {
            $this->ajax_response_add_teacher("NOK","Inconnu");
            return;
        }
        // check is not already in table teacher
        $dup = $this->cn->get_value("select use_id from extended_admin.teacher
            where
                use_id=$1", array($id));
        if ( $dup != "") {
            $this->ajax_response_add_teacher("NOK","duplicate");
            return;
        }
        // insert it into extended_admin.teacher
        $this->cn->get_value("insert into extended_admin.teacher(use_id) 
            values ($1) ",array($id));
        $ateacher=$this->cn->get_array("select use_id,use_login  as login
            from ac_users
                where use_id=$1",array($id));
        ob_start();
        $this->display_teacher($ateacher[0]);
        $response=ob_get_contents();
        ob_clean();
        $this->ajax_response_add_teacher("OK",$response,$id);
                
    }
    /**
     * @brief remove a teacher from the SQL table extended_admin.teacher
     */
    function remove_teacher($p_id)
    {
        $this->cn->exec_sql("delete from extended_admin.teacher where use_id=$1",
                array($p_id));
    }
    function ajax_response_add_teacher($p_status,$p_message,$p_id=0)
    {
        $dom=new DOMDocument("1.0");
        $root=$dom->createElement("root");
        $display=$dom->createElement("display",$p_message);
        $status=$dom->createElement("status",$p_status);
        $id=$dom->createElement("id",$p_id);
        $root->appendChild($status);
        $root->appendChild($display);
        $root->appendChild($id);
        $dom->appendChild($root);
        header('Content-type: text/xml; charset=UTF-8');
        echo $dom->saveXML();
    }

}
