<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
require_once GESTION.'/database/class_extended_admin_tag_sql.php';
require_once GESTION.'/class/class_gestion_dossier.php';

/**
 * @file
 * @brief 
 * Manage the tag
 */
class Gestion_Tag 
{
    private $cn  ;  //!< Database connexion
    private $tag ; //!< Row of table tag
    function __construct(Database $p_cn,$tag_id=-1)
    {
        $this->cn=$p_cn;
        $this->tag = new Extented_Admin_Tag_SQL($p_cn,$tag_id);
    }
    function display_list()
    {
        $x=new Extented_Admin_Tag_SQL($this->cn);
        $array=$x->collect_objects(' order by tag_code');
        require_once GESTION.'/template/tag_display_list.php';
    }
    function display_tag() {
        echo HtmlInput::hidden('tag_id',$this->tag->id);
        ?>
<label for ="tag_code">Code</label>
<input type="text" class="input_text" id="tag_code" name="tag_code" value="<?php echo $this->tag->getp('code')?>" >
<label for ="tag_description">Description</label>
<input type="text" class="input_text" id="tag_description" name="tag_description" value="<?php echo $this->tag->getp('description')?>">
<?php
    }
    /**
     * Compose and the XML answer , called by the file ajax_tag.php
     * \param $p_result OK or NOK
     * \param $p_display Contents the HTML string to display
     * \see ajax_tag.php
     */
    function ajax_response_xml($p_result,$p_display) 
    {
        $xml = new DOMDocument('1.0','utf-8');
        $root=$xml->createElement('root');
        $result=$xml->createElement('result',$p_result);
        $display=$xml->createElement('display',$p_display);
        $id=$xml->createElement('id',$this->tag->id);
        
        $root->appendChild($result);
        $root->appendChild($display);
        $root->appendChild($id);
        $xml->appendChild($root);
        header('Content-type: text/xml; charset=UTF-8');

        echo $xml->saveXML();
    }
    /**
     * Insert a new tag
     * @param $p_code tag_code
     * @param $p_description tag_description
     */
    function insert($p_code,$p_description) {
        $this->tag->setp('code',$p_code);
        $this->tag->setp('description',$p_description);
        $this->tag->insert();
    }
    /**
     * Update a tag
     * @param $p_code tag_code
     * @param $p_description tag_description
     */
    function save($p_code,$p_description) {
    
        $this->tag->setp('code',$p_code);
        $this->tag->setp('description',$p_description);
        $this->tag->save();
        
            // Update description of dossier
        $a_dossier=$this->cn->get_array("select dos_id from extended_admin.dossier_tag where tag_id=$1",
                array($this->tag->id));
        $nb_dossier=count($a_dossier);
        for ( $i=0 ; $i < $nb_dossier;$i++) {
            $dossier=new Gestion_Dossier($this->cn);
            $dossier->set_folder($a_dossier[$i]['dos_id']);
            $dossier->update_description();
        }
    }
    /**
     * Display the tag given in parameter , if the tag is null take the one from
     * the current object
     * @param Extented_Admin_Tag_SQL $p_tag
     */
    function display_row(Extented_Admin_Tag_SQL $p_tag = null) {
        static $x=0;
        if ( $p_tag == null ){
            $p_tag=$this->tag;
        }
        $class=($x%2==0)?' class="even"':'';
        $x++;
        echo '<tr id="tag_id',$p_tag->getp('id'),'" '.$class.' >';
        echo '<td>'.h($p_tag->getp('code')).'</td>';
        echo '<td>'.h($p_tag->getp('description')).'</td>';
        echo '<td>'.HtmlInput::anchor('Détail',"",'onclick="Tag.display('.$p_tag->getp('id').')"').'<td>';
        echo '<td>'.HtmlInput::anchor('Efface',"",'onclick="Tag.delete('.$p_tag->getp('id').')"').'<td>';
        echo '</tr>';
                
    }
    function delete() {

        $a_dossier=$this->cn->get_array("select dos_id from extended_admin.dossier_tag where tag_id=$1",
                array($this->tag->id));
        
        $tag_id=$this->tag_id;
        
        
        $nb_dossier=count($a_dossier);
        for ($i=0;$i<$nb_dossier;$i++) {
            $dossier=new Gestion_Dossier($this->cn);
            $dossier->set_folder($a_dossier[$i]['dos_id']);
            $dossier->remove_teacher();
            $dossier->update_description(true);
        }
        $this->tag->delete();
    }
    /**
     * \brief display a list of tags , if the type is m , showing only the unused
     * tag for template
     * @param $p_type d or m
     */
    function select_list($p_type,$p_mod_id)
    {
        $x=new Extented_Admin_Tag_SQL($this->cn);
        if ($p_type == 'm') 
        {
            $filter=" where id not in (select tag_id from extended_admin.modele_tag )";
            $array=$x->collect_objects($filter.' order by tag_code');
            require_once GESTION.'/template/tag_select_list_modele.php';
        }else {
            $array=$x->collect_objects(' order by tag_code');
            require_once GESTION.'/template/tag_select_list.php';
        }
    }
}
?>
