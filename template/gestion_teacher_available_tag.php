<h1>Choix étiquette</h1>
Recherche <?php echo HtmlInput::filter_table('gestion_teacher_available_tag_tb_id','0,1',1);?>
<table id="gestion_teacher_available_tag_tb_id">
    <tr>
        <th>Code</th>
        <th>Description</th>
    </tr>
    <?php
    $nb_array=count($array);
    for ($i=0;$i<$nb_array;$i++):
    ?>
    <tr>
        <td>
            <a href="javascript:void(0)" onclick="Teacher.tag_add('<?php echo $p_use_id?>','<?php echo $array[$i]['tag_id']?>')">
            <?php echo h($array[$i]['tag_code'])?>
            </a>
                
        </td>
        <td>
            <?php echo h($array[$i]['tag_description'])?>
        </td>
    </tr>    
    <?php
    endfor;
    ?>
</table>
<ul class="button_choice">
    <li>
        <?php echo HtmlInput::button_close('display_tag_available_id')?>
    </li>
</ul>