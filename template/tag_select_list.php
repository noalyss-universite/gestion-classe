<?php
    $nb=count($array);
    if ( $nb == 0 ) :
?>
Désolé , aucune étiquette disponible
<?php
    return;
    endif;
    ?>
Recherche <?php echo HtmlInput::filter_table('tag_select_table','0,1',1);?>

<table id="tag_select_table">
    <tr>
        <th>
            Code
        </th>   
        <th>
            Description
        </th>
    </tr>
    <?php

    for ($i=0;$i<$nb;$i++):
    ?>
    <tr>
        <td>
           <a onclick="$('tag_selected').value=<?php echo $array[$i]->id?>;$('tag_code_selected').innerHTML='<?php echo $array[$i]->tag_code?>';$('tag_code_selected').removeClassName('notice');$('tag_code_selected').show();return false;" href="javascript:void(0)">
            <?php echo $array[$i]->tag_code?>    
           </a>
        </td>
        <td>
            <?php echo $array[$i]->tag_description?>    
        </td>
    </tr>
        
    <?php 
        endfor;
    ?>
</table>

<ul class="button_choice">
    <li>
        <?php echo HtmlInput::button_close('select_tag_box')?>
    </li>
</ul>