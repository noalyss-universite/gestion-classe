<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
require_once GESTION.'/class/class_gestion_teacher.php';
require_once GESTION.'/database/class_extended_admin_teacher_tag_sql.php';
require_once NOALYSS_INCLUDE.'/lib/html_input.class.php';
require_once NOALYSS_INCLUDE.'/lib/ibutton.class.php';

$teacher = new Gestion_Teacher($cn);
//--------------------------------------------------------------------------
// Add a tag to a teacher
// teacher_tag_add
//---------------------------------------------------------------------------
if ( $action == 'teacher_tag_add') {
    $use_id=HtmlInput::default_value_get('use_id',-1);
    $tag_id=HtmlInput::default_value_get('tag_id',-1);
    $teacher->set_tag($tag_id);
    $teacher->set_teacher($use_id);
    $teacher->add();
    $teacher->ajax_response_xml("OK",$teacher->get_tag_code().
            HtmlInput::button_action('x',sprintf("Teacher.tag_remove('%s')",$teacher->get_teacher_tag_id()),'x','tinybutton'));
    
    return;
}


//--------------------------------------------------------------------------
// Display a box with the available tag for this teacher
//teacher_tag_select
//---------------------------------------------------------------------------
if ( $action == 'teacher_tag_select') {
    $id=HtmlInput::default_value_get('use_id',-1);
    $teacher->show_available_tag($id);
    return;
}



//--------------------------------------------------------------------------
// Remove a tag from the teacher
//teacher_tag_remove
//---------------------------------------------------------------------------
if ( $action == 'teacher_tag_remove') {
   
    $id=HtmlInput::default_value_get('teacher_tag_id',-1);
    $teacher->set_teacher_tag($id);
    $teacher->remove();
     
    return;
}
//--------------------------------------------------------------------------
// Remove a teacher
//
//--------------------------------------------------------------------------
if ( $action == "ajax_remove_teacher")  {
    try  {
        $teacher_id=HtmlInput::default_value_get('teacher_id',0);
        $teacher->remove_teacher($teacher_id);
        echo "OK";
    } catch(Exception $e) {
        echo $e->getMessage();
    }
    return;
}
//--------------------------------------------------------------------------
// Display a form for choosing a new teacher
//
//--------------------------------------------------------------------------
if ( $action == "ajax_new_teacher")  {
    $teacher->new_teacher();
    return;
}
//--------------------------------------------------------------------------
// Save the selected teacher , if fails return an error message , otherwise
// return the div to display
//
//--------------------------------------------------------------------------
if ( $action == "ajax_add_teacher")  {
    $new_teacher=HtmlInput::default_value_request("teacher_login","");
    $teacher->add_teacher($new_teacher);
    return;
}
//--------------------------------------------------------------------------
// Respond to the autocomplete , send a unordered list of users , active 
// and not yet in the teacher table
//--------------------------------------------------------------------------
if ( $action == "findTeacher")  {
    $param=HtmlInput::default_value_request("cteacher","XXXXXXXX");
    $alist=$cn->get_array("select use_id, use_login ,
        use_name,
        use_first_name
        from ac_users 
        where 
            use_id not in (select use_id from extended_admin.teacher)
            and use_active=1
            and use_login ilike $1||'%'
            ",array($param));
    $nb_list=count($alist);
    echo "<ul>";
    for ($i=0;$i < $nb_list;$i++) {
        printf ('<li id="%s"> %s <span class="informal">%s</span>'
                . '</li>',$alist[$i]['use_id'],$alist[$i]['use_login'],
                h($alist[$i]['use_name']),h($alist[$i]['use_first_name']));
    }
    echo "</ul>";
    return;
}