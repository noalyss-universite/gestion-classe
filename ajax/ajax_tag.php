<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))
    die('Appel direct ne sont pas permis');
require_once NOALYSS_INCLUDE.'/lib/ibutton.class.php';
require_once GESTION.'/class/class_gestion_tag.php';
require_once GESTION.'/class/class_gestion_dossier.php';
require_once GESTION.'/class/class_gestion_modele.php';
require_once GESTION.'/class/class_gestion_teacher.php';
 /**
 * @file
 * @brief 
 * Answer a ajax call
 */
///////////////////////////////////////////////////////////////////////////////
// Display the details of tag for inserting or updating
///////////////////////////////////////////////////////////////////////////////
if ( $action == 'ajax_tag_display') 
{
    $id=HtmlInput::default_value_get('tag_id',-1);
  
    if ( $id==-1) 
        $ajax_action = 'Tag.insert()';
    else
    {
        $ajax_action='Tag.save()';
        
    }
    ?>
<h2> Etiquette</h2>
<form id="tag_display_box_frm" >
    <?php
        $x=new Gestion_Tag($cn,$id);
        $x->display_tag();
    ?>
<p>
<ul class="button_choice">
    <li>
    <input type="submit" value="Sauver" class="smallbutton" onclick="<?php echo $ajax_action;?>;return false;">
    </li>

    <li>
        <input type="button" value="Fermer" class="smallbutton" onclick=" document.body.removeChild($('tag_display_box'))">
    </li>
</ul>
</p>
</form>
<?php
return;
    }
///////////////////////////////////////////////////////////////////////////////    
// Insert a new tag
///////////////////////////////////////////////////////////////////////////////
if ( $action == 'ajax_tag_insert') 
{   
   
    
    // retrieve variable
    $code = HtmlInput::default_value_get('tag_code','');
    $description = HtmlInput::default_value_get('tag_description','');
    $tag=new Gestion_Tag($cn);
    if ($code == '')  {
        $tag->ajax_response_xml('NOK','le code ne peut être vide');
        return;
    } 
    
    // check that this tag code doesn't exist (case insensitive) 
    $dup  = $cn->get_value(" select count(*) from extended_admin.tag where lower(tag_code)=lower($1)",
            array($code));
    
    if ( $dup != 0 ) {
        $tag->ajax_response_xml('NOK','Ce code existe déjà');
        return;
    }
    
    // Insert into DB
    $tag->insert($code,$description);

    // Get string for a row
    ob_start();
    $tag->display_row();
    $display=ob_get_clean();
    
    // Compose the XML answer : result id and display
    $tag->ajax_response_xml("OK", $display);
    

    
}    
///////////////////////////////////////////////////////////////////////////////
// Update a tag
///////////////////////////////////////////////////////////////////////////////
if ( $action == 'ajax_tag_save') 
{   
    // retrieve variable
    $code = HtmlInput::default_value_get('tag_code','');
    $id = HtmlInput::default_value_get('tag_id','0');
    $description = HtmlInput::default_value_get('tag_description','');
    $tag=new Gestion_Tag($cn,$id);
    if ($code == '')  {
        $tag->ajax_response_xml('NOK','le code ne peut être vide');
        return;
    } 
    
    // check that this tag code doesn't exist (case insensitive) 
    $dup  = $cn->get_value(" select count(*) from extended_admin.tag where lower(tag_code)=lower($1) and id != $2",
            array($code,$id));
    
    if ( $dup != 0 ) {
        $tag->ajax_response_xml('NOK','Ce code existe déjà');
        return;
    }
    
    // Insert into DB
    $tag->save($code,$description);

    // Get string for a row
    ob_start();
    $tag->display_row();
    $display=ob_get_clean();
    // Compose the XML answer : result id and display
    $tag->ajax_response_xml("OK", $display);
    

    
}
///////////////////////////////////////////////////////////////////////////////
// Delete a tag
///////////////////////////////////////////////////////////////////////////////
if ( $action == 'ajax_tag_delete') {
    $id=HtmlInput::default_value_get('tag_id',0);
    $tag=new Gestion_Tag($cn,$id);
    $tag->delete();
}
///////////////////////////////////////////////////////////////////////////////
// Show a list of tags and let the user select one of them 
// 
///////////////////////////////////////////////////////////////////////////////
if ( $action == 'ajax_tag_select') {
    $tag = new Gestion_Tag($cn);
    echo '<h2>'.'Choisissez une étiquette'.'</h2>';
    $type=HtmlInput::default_value_get('type','');
    $mod_id=HtmlInput::default_value_get('mod_id','');
    $tag->select_list($type,$mod_id);
}
///////////////////////////////////////////////////////////////////////////////
// Set the tag on the folder or template
///////////////////////////////////////////////////////////////////////////////
if ( $action == 'ajax_tag_set') {
    $id=HtmlInput::default_value_get('p_id',-1);
    $tag=HtmlInput::default_value_get('tag',"");
    $type=HtmlInput::default_value_get('type','');
    $act=HtmlInput::default_value_get('action','');
    // $type is d for folder (dossier) m for template
    $teacher=new Gestion_Teacher($cn);
                        
    switch ($type)
    {
        case 'd':
            ////////////////////////////////////////
            // Mark for deletion or tag a folder
            ////////////////////////////////////////
            $dos=new Gestion_Dossier($cn);
            $dos->set_folder($id);
            if ( $tag != 'drop') 
            {
                if ( $dos->get_drop()=='Y') { 
                    echo "Effacement à valider";
                    return;
                }
               // $teacher->remove_access_folder($id);
                $dos->set_tag($tag);
                //$teacher->set_access_folder($id);
                $tag=new Extented_Admin_Tag_SQL($cn,$tag);
                $tag->load();
                echo $tag->getp('code');
            } else {
                if ($id != -1)
                {// Mark for drop
                    $dos->mark_drop();
                    if ( $dos->get_drop()== 'Y')
                        echo "Effacement à valider";
                    else {
                        $tag=new Extented_Admin_Tag_SQL($cn,$dos->get_tag());
                        $tag->load();
                        $content = ($tag->id==-1)?"aucune":$tag->getp('code');
                        echo $content;
                    }
                        
                }
            }
            break;
        case 'm':
            ////////////////////////////////////////
            // Mark for deletion or tag a template 
            ////////////////////////////////////////
            $mod=new Gestion_Modele($cn);
            // We set a new tag
            if ( $tag != 'drop') 
            {
                
                if ( $mod->get_drop()=='Y') {
                    echo "Effacement à valider";
                    return;
                }
                $mod_id=$id;
                $mod->set_modele($id);
                $mod_tag_id=HtmlInput::default_value_get("mod_tag_id",'');
                switch ($act) {
            // for set , 
                    case 'set':
                        $mod->set_id($mod_tag_id);
                        $mod->set_tag($tag);
                        break;
            // for del , 
                    case 'del':
                        $mod->set_id($mod_tag_id);
                        $mod->remove_tag();
                        break;
            // for add , 
                    case 'add':
                        $mod->set_modele($id);
                        $mod->add_tag($tag);
                        $mod_id=$id;
                        break;
                    default:
                        echo "erreur";
                }
                // Display the row
                
                $array=$mod->get_array($mod_id);
               echo  $mod->display_cell_tag($array);
            } else {
                if ($id != -1)
                {// Mark for drop
                    $mod->set_modele($id);
                    if ( $mod->get_drop() == 'N') {
                        echo "Effacement à valider";
                        $mod->mark_drop();
                    } else {
                        $mod->mark_undrop();
                        $array=$mod->get_array($id);
                        $mod->display_cell_tag($array);
                    }
                                            
                }
            }
            break;
            

        default:
            echo "ERREUR PARAMETRE";
            break;
    }
}

?>
