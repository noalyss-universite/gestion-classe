<?php
/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>
/**
 * @file Manage ajax for Student
 * 
 */

/*
 * Manage the activate / deactivate / suppress for students
 */
if ($action=="ajax_remove_student")
{
    $action_lib=array(1=>"Activer", 2=>"Désactiver", 3=>"Effacer");
    $action_ajax=HtmlInput::default_value_post("action", 0);
    require_once NOALYSS_INCLUDE."/lib/ibutton.class.php";

    if (isNumber($action_ajax)!=1||$action_ajax<1||$action_ajax>4)
    {
        echo h2("Action invalide");
        echo HtmlInput::button_close("student_confirm");
        return;
    }

    echo h2($action_lib[$action_ajax]);
    $student=HtmlInput::default_value_post("student", "");
    if ($student==""||empty($student))
    {
        echo h2("Aucun étudiant choisi");
        echo HtmlInput::button_close("student_confirm");
        return;
    }

    $nb_student=count($student);
    ?>
    <form method="POST">
        <input type="hidden" name="do" value="student">
        <input type="hidden" name="action" value="<?php echo $action_ajax ?>">
        <table>
            <tr>
                <th>login</th>
                <th>nom</th>
                <th>Prénom</th>
                <th></th>
            </tr>
            <?php
            for ($i=0; $i<$nb_student; $i++)
            {
                $student_detail=$cn->get_array("select use_name,use_first_name,use_login
            from ac_users 
            where
            use_id=$1", array($student[$i]));
                if (empty($student_detail))
                    continue;
                printf('<tr id="stu%d">', $student[$i]);
                echo "<td>",
                HtmlInput::hidden('student[]', $student[$i]),
                h($student_detail[0]["use_login"]),
                "</td>";
                echo td($student_detail[0]["use_name"]);
                echo td($student_detail[0]["use_first_name"]);
                echo "<td>",
                HtmlInput::anchor("Enlever", "",
                        sprintf('onclick="$(\'stu%d\').remove()"', $student[$i])
                );
                echo "</td>";
                echo "</tr>";
            }
            ?>

        </table>
        <ul class="button_choice">
            <li>
    <?php echo HtmlInput::submit("validate", "Confirmer") ?>
            </li>
            <li>
    <?php echo HtmlInput::button_close("student_confirm"); ?>
            </li>
        </ul>
    </form>
    <?php
}
?>