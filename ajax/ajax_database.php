<?php

/*
 *   This file is part of NOALYSS.
 *
 *   NOALYSS is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   NOALYSS is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with NOALYSS; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
// Copyright (2016) Author Dany De Bontridder <dany@alchimerys.be>

if (!defined('ALLOWED'))     die('Appel direct ne sont pas permis');
/*----------------------------------------------------------------------------
 *  Propose to confirm the drop of database
 *
 *----------------------------------------------------------------------------*/
if  ($action == 'ajax_drop_confirm') 
{
    $a_database=$cn->get_array(
            "
                select dos_id as id, dos_name as name,dos_description as desc,'D' as dbtype
                from
                ac_dossier
                where 
                dos_id in (select dos_id from extended_admin.dossier_tag where dt_drop='Y')
                union all 
                select mod_id,mod_name,mod_desc , 'M' as dbtype
                from public.modeledef
                where 
                mod_id in (select mod_id from extended_admin.modele_tag where mt_drop='Y')
                order by 2
             ");
$nb_database = count($a_database);
$a_type=array('D'=>'Dossier','M'=>'Modèle');
    ?>
<h2>Confirmation effacement base de données</h2>
Recherche <?php echo HtmlInput::filter_table('database_drop_tb','1','0,1,2');?>
<table id="database_drop_tb">
    <tr>
	<th>
	    id
	</th>
        <th>
            Nom
        </th>
        <th>
            Description
        </th>
        <th>
            Type
        </th>
    </tr>
    <?php
    for ( $i = 0;$i< $nb_database;$i++):
    ?>
    <tr>
	<td>
            <?php echo h($a_database[$i]['id']);?>
	</td>
        <td>
            <?php echo h($a_database[$i]['name']);?>
        </td>
        <td>
            <?php echo h($a_database[$i]['desc']);?>
        </td>
        <td>
            <?php echo h($a_type[$a_database[$i]['dbtype']]);?>
        </td>
    </tr>
    <?php 
    endfor;
    ?>
</table>
<ul class="button_choice">
    <li>
        <INPUT type="button" class="smallbutton" value="Confirmer" onclick="Database.dropthem()">
    </li>
    <li>
        <INPUT type="button" class="smallbutton" value="Fermer" onclick="$('confirm_drop_div').remove()">
    </li>
</ul>
<?php
return;
}
/*----------------------------------------------------------------------------
 *  Drop is confirmed so drop them
 *
 *----------------------------------------------------------------------------*/
if ( $action == 'ajax_drop') 
{
    $a_database=$cn->get_array(
            "
                select dos_id as id, dos_name as name,dos_description as desc,'D' as dbtype
                from
                ac_dossier
                where 
                dos_id in (select dos_id from extended_admin.dossier_tag where dt_drop='Y')
                union all 
                select mod_id,mod_name,mod_desc , 'M' as dbtype
                from public.modeledef
                where 
                mod_id in (select mod_id from extended_admin.modele_tag where mt_drop='Y')
                order by 2
             ");
    $nb_database = count($a_database);
    $a_type=array('D'=>'dossier','M'=>'mod');

    for ($i=0;$i<$nb_database;$i++) {
        $database=$a_database[$i];
        $type=$database['dbtype'];
        
        $sql=' drop database if exists '.domaine.$a_type[$type].$database['id'].' ';
        try {
            $cn->exec_sql($sql);
            if ($type=='D') {
                $cn->exec_sql("delete from  jnt_use_dos where dos_id=$1",array($database['id']));
                $cn->exec_sql(' delete from ac_dossier where dos_id=$1',array($database['id']));
            } elseif ( $type == 'M') {
                $cn->exec_sql(' delete from modeledef where mod_id=$1',array($database['id']));
            }
            echo '<li> Effacement réussi '.h($database['name']).' '.h($database['desc']).'</li> ';
        } catch (Exception $ex) {
            echo '<li> Effacement échec'.h($database['name']).' '.h($database['desc']).'</li> ';
        }
    }
    ?>
<ul class="button_choice">
    <li>
        <INPUT type="button" class="smallbutton" value="Fermer" onclick="$('confirm_drop_div').remove()">
    </li>
</ul>
<?php
    return;    
}
